# Pronounceable Generator

This is a small tool for generating strings/words/names using simple expansion and presets.

It's available on Gitlab pages here: https://samsch.gitlab.io/pronounceable-generator/

## Run instructions

### Initial setup

Install dependencies:

```
npm install
```

Build the static assets:

```
npm run build
```

### Run command

Run a local server with

```
npm start
```

## Usage

Pattern string input takes plain characters or "patterns", which are pretty much like shell expansions. `S{a,i}ck` expands to `"Sack", "Sick"`. `{a..d}` would expand to the letters `"a", "b", "c", "d"`.

There's no limits on the pattern, and it runs in your browser, so if you make something that would expand to millions of results, you'll freeze your tab.

Presets are direct replacement of whatever the placeholder text is, with each character in the Preset value. So one of the default presets is `{v}` and `aeiouy`. That means in your pattern, using `{v}` is equivalent to using `{a,e,i,o,u,y}`. The placeholders don't need to use curly braces, it's a direct string replacement.

### Example

The pattern `Lu{c}{a,i}` uses the default `{c}` preset for consonants, and an expansion for `a` and `i`. It generates this list:

Luba, Lubi, Luca, Luci, Luda, Ludi, Lufa, Lufi, Luga, Lugi, Luha, Luhi, Luja, Luji, Luka, Luki, Lula, Luli, Luma, Lumi, Luna, Luni, Lupa, Lupi, Luqa, Luqi, Lura, Luri, Lusa, Lusi, Luta, Luti, Luva, Luvi, Luwa, Luwi, Luxa, Luxi, Luya, Luyi, Luza, Luzi

(In the app it applies some layout styles to make it easier to read though.)

### Saved stuff

After you hit run for a pattern, the pattern is added to the URL, and if on a public server you can link to it. HOWEVER:

Presets are saved in localStorage, so they can't currently be included in links. A new person opening the app will only have the default presets `{v}` and `{c}`. If you send a link with a missing preset, it won't be replaced and will instead be part of the pattern.

## Development instructions

Install deps

```
npm install
```

You can run the dev build once with

```
npm run dev
```

Or in watch mode with 

```
npm run dev -- --watch
```

Watch mode does not automatically reload the page.

### Notes

The source is in `/custom/src` for historical reasons (there was originally an Express server in this project, and the client side stuff was just one of the pages).

## "Deployment"

If you really wanted to you could dump the `public/` folder in a public web server. `public/index.html` uses relative paths for the assets.
