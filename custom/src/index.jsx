'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const expand = require('brace-expansion');
const getInitialQuery = require('./getInitialQuery');
const usePresetsState = require('./usePresetsState');
const Presets = require('./Presets');
const Instructions = require('./Instructions');

require('./styles.css');

const { useState, useMemo } = React;

const initial = getInitialQuery();

const App = () => {
	const [input, setInput] = useState(initial);
	const [current, setCurrent] = useState(initial);
	const { outputString, presetsProps } = usePresetsState(current);

	const list = useMemo(() => {
		return expand(outputString);
	}, [current]);

	return (
		<div>
			<Instructions />
			<Presets {...presetsProps} />
			<div>
				<h2>Input {input !== current ? '(Changed)' : ''}</h2>
				<form
					onSubmit={event => {
						event.preventDefault();
						setCurrent(input);
						// eslint-disable-next-line no-restricted-globals
						history.pushState({}, document.title, `?input=${input}`);
					}}
				>
					<label htmlFor="pattern-input">Pattern string</label>{' '}
					<input
						className="pattern-input"
						type="text"
						id="pattern-input"
						value={input}
						onChange={event => {
							setInput(event.target.value);
						}}
					/>{' '}
					<button type="submit">Run</button>
				</form>
			</div>
			<div>
				<h2>Output</h2>
				<div className="list">
					{list.map(item => (
						<div>{item}</div>
					))}
				</div>
			</div>
		</div>
	);
};

ReactDOM.render(<App />, document.getElementById('root'));
