'use strict';

const React = require('react');
const Preset = require('./Preset');

function Presets({ presetList, updatePreset, addPreset, removePreset }) {
	return (
		<div>
			<h2>Presets</h2>
			<div>
				{presetList.map(preset => {
					return (
						<Preset
							id={preset.id}
							placeholder={preset.placeholder}
							letters={preset.letters}
							updatePreset={updatePreset}
							deletePreset={() => {
								removePreset(preset.id);
							}}
						/>
					);
				})}
			</div>
			<div>
				<button
					type="button"
					onClick={() => {
						addPreset();
					}}
				>
					Add Preset
				</button>
			</div>
		</div>
	);
}

module.exports = Presets;
