'use strict';

const React = require('react');

function Preset({ id, placeholder, letters, updatePreset }) {
	return (
		<div className="preset">
			<div>
				<label htmlFor={`placeholder-input-${id}`}>Placeholder</label>
				<input
					type="text"
					id={`placeholder-input-${id}`}
					value={placeholder}
					onChange={event => {
						updatePreset(id, event.target.value, letters);
					}}
				/>
			</div>
			<div>
				<label htmlFor={`preset-input-${id}`}>Preset</label>
				<input
					type="text"
					id={`preset-input-${id}`}
					value={letters}
					onChange={event => {
						updatePreset(id, placeholder, event.target.value);
					}}
				/>
			</div>
		</div>
	);
}

module.exports = Preset;
