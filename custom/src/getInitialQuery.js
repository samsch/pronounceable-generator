'use strict';

module.exports = function getInitialQuery() {
	const params = new URLSearchParams(document.location.search.substring(1));
	const input = params.get('input');

	if (typeof input === 'string') {
		return input;
	}
	return 'Es{r..t}{a,e,i,o,u,y}m';
};
