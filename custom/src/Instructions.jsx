'use strict';

const React = require('react');

const { useState } = React;

function Instructions() {
	const [open, setOpen] = useState(false);
	return (
		<div>
			<button
				type="button"
				onClick={() => {
					// Usually want to use updator form, but here we want the latest *rendered* UI state
					// to be the actual decider of what happens. AKA, several clicks on a frozen page
					// shouldn't make it toggle a bunch of times.
					setOpen(!open);
				}}
			>
				{open ? 'Hide instructions' : 'Show instructions'}
			</button>
			{open ? (
				/* eslint-disable react/no-unescaped-entities */
				// prettier-ignore
				<div>
					<p>
						Pattern string input takes plain characters or "patterns", which are pretty much
						like shell expansions. <code>S{'{'}a,i{'}'}ck</code> expands to <code>"Sack",
						"Sick"</code>. <code>{'{'}a..d{'}'}</code> would expand to the letters <code>"a",
						"b", "c", "d"</code>.
					</p>
					<p>
						There's no limits on the pattern, and it runs in your browser, so if you make something
						that would expand to millions of results, you'll freeze your tab.
					</p>
					<p>
						Presets are direct replacement of whatever the placeholder text is, with each character
						in the Preset value. So one of the default presets is <code>{'{'}v{'}'}</code> and
						{' '}<code>aeiouy</code>. That means in your pattern, using <code>{'{'}v{'}'}</code> is
						equivalent to using <code>{'{'}a,e,i,o,u,y{'}'}</code>. The placeholders don't need to
						use curly braces, it's a direct string replacement.
					</p>
					<p>
						You can find the source for this app
						at <a href="https://gitlab.com/samsch/pronounceable-generator">https://gitlab.com/samsch/pronounceable-generator</a>
					</p>
				</div>
			) : null}
		</div>
	);
}

module.exports = Instructions;
