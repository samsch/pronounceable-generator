'use strict';

const { useState, useMemo, useEffect } = require('react');

const localStorageKey = 'pronounceableGeneratorUsePresetState';

const initialPresetsList = (() => {
	try {
		const savedList = JSON.parse(localStorage.getItem(localStorageKey));
		if (Array.isArray(savedList) !== true) {
			throw new Error('nope, not it');
		}
		return savedList;
	} catch (error) {
		return [
			{
				id: 'vowels-initial',
				placeholder: '{v}',
				letters: 'aeiouy',
			},
			{
				id: 'consonants',
				placeholder: '{c}',
				letters: 'bcdfghjklmnpqrstvwxyz',
			},
		];
	}
})();

function usePresetsState(baseString) {
	const [presetList, setPresetList] = useState(initialPresetsList);
	function updatePreset(id, placeholder, letters) {
		setPresetList(current => {
			return current.map(preset => {
				if (preset.id === id) {
					return {
						...preset,
						placeholder,
						letters,
					};
				}
				return preset;
			});
		});
	}
	function addPreset() {
		setPresetList(current => {
			return current.concat({
				id: `${Date.now()}-${Math.random()}`,
				placeholder: '',
				letters: '',
			});
		});
	}
	function removePreset(id) {
		setPresetList(current => {
			return current.flatMap(preset => {
				if (preset.id === id) {
					return [];
				}
				return preset;
			});
		});
	}
	useEffect(() => {
		localStorage.setItem(localStorageKey, JSON.stringify(presetList));
	}, [presetList]);
	const outputString = useMemo(() => {
		return presetList.reduce((inputString, preset) => {
			return inputString.replaceAll(
				preset.placeholder,
				`{${preset.letters.split('').join(',')}}`,
			);
		}, baseString);
	}, [baseString, presetList]);
	return {
		outputString,
		presetsProps: {
			presetList,
			updatePreset,
			addPreset,
			removePreset,
		},
	};
}

module.exports = usePresetsState;
